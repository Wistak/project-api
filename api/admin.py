from django.contrib import admin
from api.models import Insumo, Sucursal, Inventario, Pedido
# Register your models here.

admin.site.register(Insumo)
admin.site.register(Sucursal)
admin.site.register(Inventario)
admin.site.register(Pedido)

