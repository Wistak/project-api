from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import InsumoSerializer, SucursalSerializer, InventarioSerializer, PedidoSerializer
from .models import Insumo, Sucursal, Inventario, Pedido


# INSUMOS #

@api_view(['GET'])
def apiOverView(request):
    api_urls = {
        'List':'task-list',
        'Detail':'detail-view',
    }

    return Response(api_urls)


@api_view(['GET'])
def insumoList(request):
    insumos = Insumo.objects.all()
    serializer = InsumoSerializer(insumos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def insumoDetail(request, pk):
    insumos = Insumo.objects.get(id=pk)
    serializer = InsumoSerializer(insumos, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def insumoCreate(request):
    serializer = InsumoSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


@api_view(['POST'])
def insumoUpdate(request, pk):
    insumo = Insumo.objects.get(id=pk)
    serializer = InsumoSerializer(instance=insumo, data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


@api_view(['POST'])
def insumoDelete(request, pk):
    insumos = Insumo.objects.get(id=pk)
    insumos.delete()

    return Response('Borrado Exitosamente')
    
# FINALIZA INSUMOS #

# PEDIDOS #


@api_view(['GET'])
def pedidoList(request):
    pedidos = Pedido.objects.all()
    serializer = PedidoSerializer(pedidos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def pedidoDetail(request, pk):
    pedidos = Pedido.objects.get(id=pk)
    serializer = PedidoSerializer(pedidos, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def pedidoCreate(request):
    serializer = PedidoSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


@api_view(['POST'])
def pedidoUpdate(request, pk):
    pedidos = Pedido.objects.get(id=pk)
    serializer = PedidoSerializer(instance=pedidos, data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


@api_view(['POST'])
def pedidoDelete(request, pk):
    pedidos = Pedido.objects.get(id=pk)
    pedidos.delete()

    return Response('Borrado Exitosamente')


# SUCURSALES #
@api_view(['GET'])
def sucursalDetail(request, pk):
    sucursales = Sucursal.objects.get(id=pk)
    serializer = SucursalSerializer(sucursales, many=False)
    return Response(serializer.data)