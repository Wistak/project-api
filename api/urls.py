"""restapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.insumoList, name="insumosList"),
    path('detalle/<str:pk>/', views.insumoDetail, name="insumoDetail"),
    path('editar/<str:pk>/', views.insumoUpdate, name="insumoUpdate"),
    path('crear/', views.insumoCreate, name="insumoCreate"),
    path('borrar/<str:pk>/', views.insumoDelete, name="insumoDelete"),
    path('pedidos/', views.pedidoList, name="pedidoList"),
    path('pedidos/detalle/<str:pk>/', views.pedidoDetail, name="pedidoDetail"),
    path('pedidos/editar/<str:pk>/', views.pedidoUpdate, name="pedidoUpdate"),
    path('pedidos/crear/', views.pedidoCreate, name="pedidoCreate"),
    path('pedidos/borrar/<str:pk>/', views.pedidoDetail, name="pedidoDetail"),
    path('sucursales/detalle/<str:pk>/', views.sucursalDetail, name="sucursalDetail"),

]
